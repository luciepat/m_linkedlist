#include "LinkedList.h"

void* add_place_link(int num, int place, linked_list* head){
	linked_list* temp = head;
	linked_list* _new = NULL;
	int i = 0;
	int flag = 0;
	place--; // so it works like index
	if (head == NULL && place == i) {// if they want to initialize the linked list
		head = (linked_list*)malloc(sizeof(linked_list));
		head->num = num;
		head->_next = NULL;
		return head;
	}
	else if (head == NULL) {
		printf("Trying to place link before initilization");
		return NULL;
	}

	while ( i < place &&temp&&temp->_next != NULL) {
		i++;
		temp = temp->_next;
	}

	if (i == 0) { // new head
		_new = (linked_list*)malloc(sizeof(linked_list));
		_new->num = num;
		_new->_next = head;
		return _new;
	}
	else if (i==place) {//I.E. they want to add a link to the end
		_new = (linked_list*)malloc(sizeof(linked_list));
		_new->num = num;
		_new->_next = temp->_next;
		temp->_next = _new;
	}
	else {
		printf("Out of bounds\n");
	}
	return head;
}
