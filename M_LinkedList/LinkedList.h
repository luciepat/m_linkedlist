#pragma once
#include <stdio.h>
typedef struct linked_list {
	int num;
	struct linked_list* _next;
} linked_list;

void* add_place_link(int num, int place, linked_list* head);